from django.shortcuts import render
from django.views import View

from blog.forms import BlogForm, BlogFormset, CategoryForm
from blog.models import Blog, Category


class HomeView(View):
    template_name = 'index.html'
    def get(self, request, **kwargs):
        formsingle = BlogForm()
        formset = BlogFormset(queryset=Blog.objects.all())
        return render(request, self.template_name, locals())

    def post(self, request, **kwargs):
        formset = BlogFormset(request.POST)
        for form in formset:
            if form.is_valid():
                form.save()
        return render(request, self.template_name, locals())

class CategoryView(View):
    model = Category
    template_name = 'category_create.html'
    def post(self, request, **kwargs):
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
        return render(request, 'index.html', {
            'formset': BlogFormset(queryset=Blog.objects.all())
        })
    def get(self, request, **kwargs):
        form = CategoryForm()
        return render(request, self.template_name, locals())