from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
	class Meta(AbstractUser.Meta):
		swappable = 'AUTH_USER_MODEL'

class Category(models.Model):
    name = models.CharField(max_length=34)
    def __str__(self):
        return self.name

class Blog(models.Model):
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=43)
    context = models.TextField()
    created_at = models.DateTimeField(auto_now=True)
    shared = models.BooleanField(default=True)