from django import forms
from blog.models import *
from django.forms import formset_factory, inlineformset_factory, modelformset_factory


class BlogForm(forms.ModelForm):
	class Meta:
		model = Blog
		exclude = []

class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		exclude = []

BlogFormset = modelformset_factory( Blog, exclude=[])

